import { OrderItem } from 'src/orders/entities/orderItem.entity';
import { Type } from 'src/type/entities/type.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Type, (type) => type.products)
  type: Type;

  @OneToMany(() => OrderItem, (orderItems) => orderItems.product)
  orderItems: OrderItem[];
}
